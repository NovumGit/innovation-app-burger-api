<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.burger',
    'namespace'   => 'ApiNovumBurger',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'api.burger.demo.novum.nu',
    'dev_domain'  => 'api.burger.innovatieapp.nl',
    'test_domain' => 'api.burger.test.demo.novum.nu',
];
