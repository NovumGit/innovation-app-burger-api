<?php
namespace ApiNovumBurger\Generated;

/**
 * This class is automatically generated by the build script.
 * Do not change it as your changes will be lost after the next build
 * If you need modifications please extend
 */
final class ApiInfo implements \Api\Info\IApiInfo
{
	final public function getTitle(): string
	{
		return "Dit endpoint simuleert de burger";
	}


	final public function getDescription(): string
	{
		return "
		        Fungeert als trigger voor het vullen van de overige systemen
		    ";
	}


	final public function getOrganisation(): string
	{
		return "Novum";
	}


	final public function getApiDir(): string
	{
		return "api.burger.demo.novum.nu";
	}


	final public function getEmail(): string
	{
		return "anton@novum.nu";
	}


	final public function getServers(): array
	{
		$aReturn = [];
		$aReturn[0]["description"] = "Production API";
		$aReturn[0]["url"] = "https://api.burger.demo.novum.nu";
		$aReturn[1]["description"] = "Production user application";
		$aReturn[1]["url"] = "https://admin.burger.demo.novum.nu";
		$aReturn[2]["description"] = "Test API";
		$aReturn[2]["url"] = "https://api.test.burger.demo.novum.nu";
		$aReturn[3]["description"] = "Test API documentation";
		$aReturn[3]["url"] = "https://api.test.burger.demo.novum.nu";
		$aReturn[4]["description"] = "Test user application";
		$aReturn[4]["url"] = "https://admin.test.burger.demo.novum.nu";
		return $aReturn;
	}


	final public function getContacts(): array
	{
		$aReturn = [];
		$aReturn[0]["email"] = "anton@novum.nu";
		$aReturn[0]["type"] = "TECHNICAL";
		$aReturn[0]["name"] = "Anton Boutkam";
		$aReturn[1]["email"] = "anton@novum.nu";
		$aReturn[1]["type"] = "SUPPORT";
		$aReturn[1]["name"] = "anton@novum.nu";
		return $aReturn;
	}


	final public function getEndpointUrl(): string
	{
		return "https://api.burger.demo.novum.nu";
	}


	final public function getDocumentationUrl(): string
	{
		return "https://api.burger.demo.novum.nu";
	}


	final public function getAuthorisationModel(): string
	{
		return "none";
	}


	final public function getCaCert(): string
	{
		return "";
	}
}
